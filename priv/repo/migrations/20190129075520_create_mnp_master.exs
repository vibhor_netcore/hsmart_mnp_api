defmodule Hello.Repo.Migrations.CreateMnpMaster do
  use Ecto.Migration

  def change do
    create table(:mnp_master) do
      add :ref_id, :"bigint unsigned"
      add :msisdn, :string
      add :gateway_id, :"smallint unsigned"
      add :created_date, :naive_datetime
    end

  end
end
