defmodule Hello.Mnpmaster do
  use Ecto.Schema
  import Ecto.Changeset

  schema "mnp_master" do
    field :created_date, :date
    field :gateway_id, :integer
    field :msisdn, :string
    field :ref_id, :integer
  end

  @doc false
  def changeset(mnpmaster, attrs) do
    mnpmaster
    |> cast(attrs, [:ref_id, :msisdn, :gateway_id, :created_date])
    |> validate_required([:ref_id, :msisdn, :gateway_id])
  end
end
