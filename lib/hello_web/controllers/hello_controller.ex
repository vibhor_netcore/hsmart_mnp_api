defmodule HelloWeb.HelloController do
  use HelloWeb, :controller

  alias Hello.Repo

  import Ecto.Query, only: [from: 2]

  def index(conn, _params) do

    e = Date.to_string(Date.add(Date.utc_today(),0))
    s = Date.to_string(Date.add(Date.utc_today(),-1))

    query = from u in "mnp_master",
    where: u.created_date > ^s,
    where: u.created_date < ^e,
    select: {u.msisdn,u.gateway_id}

    json(conn,%{request_id: Ecto.UUID.generate, response: Enum.map(Repo.all(query), fn {n,o}  -> %{operator: o, msisdn: n} end)})

  end

  def index2(conn, %{"date" => date}) do

    {:ok, d} = Date.from_iso8601(date)
    e = Date.to_string(Date.add(d,1))
    s = Date.to_string(d)

    query = from u in "mnp_master",
    where: u.created_date > ^s,
    where: u.created_date < ^e,
    select: {u.msisdn,u.gateway_id}

    json(conn,%{request_id: Ecto.UUID.generate, response: Enum.map(Repo.all(query), fn {n,o}  -> %{operator: o, msisdn: n} end)})

  end

end
